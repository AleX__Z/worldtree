package com.company.gui;

import com.company.lang.Node;
import com.company.lang.Tree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class MainFrame extends JFrame {
	public static final String UNKNOWN_IMAGE_PATH = "unknown.png";
	public static final String UNKNOWN_NAME = "Unknown";

	private JButton addPersonButton;
	private JPanel mainRoot;
	private JComboBox comboBox1;
	private JLabel mainPersonPhotoLabel;
	private JLabel mainPersonNameLabel;
	private JLabel motherPhotoLabel;
	private JLabel motherNameLabel;
	private JLabel fatherPhotoLabel;
	private JLabel fatherNameLabel;
	private JLabel motherMotherPhotoLabel;
	private JLabel fatherMotherPhotoLabel;
	private JLabel motherFatherPhotoLabel;
	private JLabel fatherFatherPhotoLabel;
	private JLabel motherMotherNameLabel;
	private JLabel fatherMotherNameLabel;
	private JLabel motherFatherNameLabel;
	private JLabel fatherFatherNameLabel;
	private JPanel childrenChildrenLabel;
	private JPanel childrenLabel;
	private JButton deleteButton;
	private JLabel infoLabal;

	private Tree tree = null;
	List<Node> nodes;


	public MainFrame(Tree tree) {
		super("");
		this.tree = tree;
		setContentPane(mainRoot);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("World Tree");
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setPreferredSize(new Dimension(800, 600));
		setLocationRelativeTo(null);
		childrenLabel.setLayout(new FlowLayout());
		childrenChildrenLabel.setLayout(new FlowLayout());


		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				tree.save();
			}
		});

		deleteButton.addActionListener(e -> {
			new DeleteDialog(tree);
		});

		addWindowFocusListener(new WindowFocusListener() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				SwingUtilities.invokeLater(() -> {
					nodes = tree.findPersonByName("");
					nodes.sort(new Comparator<Node>() {
						@Override
						public int compare(Node o1, Node o2) {
							return o1.getName().compareTo(o2.getName());
						}
					});
					comboBox1.setModel(new DefaultComboBoxModel<>(nodes.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
					if (comboBox1.getSelectedIndex() != -1)
						comboBox1.setSelectedIndex(0);
					else {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								drawNode(null);
							}
						});
					}
					;
				});

			}

			@Override
			public void windowLostFocus(WindowEvent e) {

			}
		});

		comboBox1.addActionListener(e -> {
			SwingUtilities.invokeLater(
					() -> drawNode(nodes.get(comboBox1.getSelectedIndex()))
			);
		});

		addPersonButton.addActionListener(e -> new AddPersonDialog(tree));

		pack();
		setVisible(true);
	}

	private void drawNode(Node node) {


		JLabel[] jLabels = new JLabel[]{
				mainPersonNameLabel, mainPersonPhotoLabel,
				motherNameLabel, motherPhotoLabel,
				fatherNameLabel, fatherPhotoLabel,
				motherMotherNameLabel, motherMotherPhotoLabel,
				fatherMotherNameLabel, fatherMotherPhotoLabel,
				motherFatherNameLabel, motherFatherPhotoLabel,
				fatherFatherNameLabel, fatherFatherPhotoLabel
		};

		if (node != null) {
			StringBuilder builder = new StringBuilder("<html>Age : " + node.age);
			builder.append("<br>Sex : ").append(node.sex.name());
			builder.append("<br>Profession : ").append((node.profession.isEmpty()) ? "Unknown" : node.profession);
			builder.append("<br>Partner : ").append(
					(node.partner == null) ?
							"Unknown" :
							node.partner.getName() +
									" " +
									node.partner.surname +
									" " +
									node.partner.firstName

			);
			builder.append("</html>");

			infoLabal.setText(builder.toString());


			List<Node> nodes = new ArrayList<>();
			nodes.add(node);
			nodes.addAll(node.getParents());
			if (node.mother != null) nodes.addAll(node.mother.getParents());
			else nodes.addAll(Arrays.asList(null, null));
			if (node.father != null) nodes.addAll(node.father.getParents());
			else nodes.addAll(Arrays.asList(null, null));

			for (int i = 0, size = nodes.size(); i < size; i++) {
				Node tmp = nodes.get(i);
				if (tmp != null) {
					jLabels[2 * i].setText(tmp.name + " " + tmp.surname + " " + tmp.firstName);
					jLabels[2 * i + 1].setIcon(getResizeImageIcon(new ImageIcon(tmp.nameFilePhoto)));

					final int finalI = i;
					jLabels[2 * i + 1].addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							((Component) e.getSource()).removeMouseListener(this);
							if (jLabels[2 * finalI + 1].getMouseListeners().length == 0)
								new PersonFrame(tree, tmp);
						}


					});
				} else {
					jLabels[2 * i].setText(UNKNOWN_NAME);
					jLabels[2 * i + 1].setIcon(getResizeImageIcon(new ImageIcon(UNKNOWN_IMAGE_PATH)));
				}
			}

			Set<Node> children = node.children;
			Set<Node> childChildrenNode = new HashSet<>();
			for (Node c : children)
				childChildrenNode.addAll(c.children);

			childrenLabel.removeAll();
			for (Node c : children) {
				childrenLabel.add(new NodeComponent(tree, c));
			}
			childrenLabel.updateUI();

			childrenChildrenLabel.removeAll();
			for (Node c : childChildrenNode) {
				childrenChildrenLabel.add(new NodeComponent(tree, c));
			}
			childrenChildrenLabel.updateUI();
		} else {
			infoLabal.setText("");
			for (int i = 0, size = jLabels.length; i < size; i++) {
				if (i % 2 == 0) {
					jLabels[i].setText(UNKNOWN_NAME);
				} else {
					jLabels[i].setIcon(getResizeImageIcon(new ImageIcon(UNKNOWN_IMAGE_PATH)));
				}
			}
			childrenLabel.removeAll();
			childrenLabel.updateUI();
			childrenChildrenLabel.removeAll();
			childrenChildrenLabel.updateUI();
		}
	}


	private static ImageIcon getResizeImageIcon(ImageIcon icon) {
		Image image = icon.getImage();
		return new ImageIcon(image.getScaledInstance(100, 100, Image.SCALE_SMOOTH));
	}

	static class NodeComponent extends JPanel {
		JLabel photoLabel;
		JLabel nameLanel;

		public NodeComponent(Tree tree, Node node) {
			super();


			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			photoLabel = new JLabel();
			nameLanel = new JLabel(String.format("%s %s %s", node.name, node.surname, node.firstName));
			nameLanel.setVerticalAlignment(SwingConstants.CENTER);
			nameLanel.setVerticalTextPosition(SwingConstants.CENTER);
			nameLanel.setHorizontalTextPosition(SwingConstants.CENTER);
			nameLanel.setHorizontalAlignment(SwingConstants.CENTER);
			photoLabel.setIcon(getResizeImageIcon(new ImageIcon(node.nameFilePhoto)));
			photoLabel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					((Component) e.getSource()).removeMouseListener(this);
					if (photoLabel.getMouseListeners().length == 0)
						new PersonFrame(tree, node);
				}


			});
			add(photoLabel);
			add(nameLanel);
			//setVisible(true);
		}
	}

}
