package com.company.gui;

import com.company.lang.Node;
import com.company.lang.Tree;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Alex on 19.11.2014.
 */
public class DeleteDialog extends JFrame {
	private JComboBox comboBox1;
	private JButton deleteButton;
	private JPanel deleteRoot;
	public Tree tree;

	public DeleteDialog(Tree tree) {
		super();
		this.tree = tree;
		setLocationRelativeTo(null);
		setContentPane(deleteRoot);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Delete Person");
		//setPreferredSize(new Dimension(200,()));


		List<Node> list = tree.findPersonByName("");
		list.sort((new Comparator<Node>() {
			@Override
			public int compare(Node o1, Node o2) {
				return o1.name.compareTo(o2.name);
			}
		}));
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				comboBox1.setModel(new DefaultComboBoxModel<>(list.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
				if (comboBox1.getSelectedItem() != null) comboBox1.setSelectedIndex(0);
			}
		});


		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox1.getSelectedItem() == null) return;
				tree.deleteNode(list.get(comboBox1.getSelectedIndex()));
				setVisible(false);
				dispose();
			}
		});


		pack();
		setVisible(true);
	}
}
