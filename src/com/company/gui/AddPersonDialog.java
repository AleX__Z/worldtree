package com.company.gui;

import com.company.lang.Node;
import com.company.lang.Sex;
import com.company.lang.Tree;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AddPersonDialog extends JFrame {
	public static final String PATH = "photo/";
	public static final String PARTNER = "partner";
	public static final String CHILD = "child";


	private JButton addButton;
	private JPanel addRoot;
	private JComboBox motherBox;
	private JTextField namePersonTextField;
	private JComboBox fatherBox;
	private JButton chooseImageButton;
	private JComboBox sexBox;
	private JTextField ageField;
	private JTextField professionField;
	private JTextField surnameField;
	private JTextField firstNameField;
	private JComboBox partnerBox;
	private JLabel errorLabel;
	private JTextField dateBornField;

	private File file = null;
	private Tree tree = null;
	private List<Node> treeNode, motherNodes, fatherNodes;

	public AddPersonDialog(Tree tree) {
		this.tree = tree;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setContentPane(addRoot);
		setTitle("New person");

		SwingUtilities.invokeLater(
				() -> {
					sexBox.setModel(new DefaultComboBoxModel<>(Sex.values()));
					treeNode = tree.findPersonByName("");
					treeNode.sort(new Comparator<Node>() {
						@Override
						public int compare(Node o1, Node o2) {
							return o1.getName().compareTo(o2.getName());
						}
					});
					motherNodes = treeNode.stream().filter(Node::isWoman).collect(Collectors.toList());
					fatherNodes = treeNode.stream().filter(Node::isMan).collect(Collectors.toList());
					motherBox.setModel(new DefaultComboBoxModel<>(motherNodes.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
					fatherBox.setModel(new DefaultComboBoxModel<>(fatherNodes.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
					partnerBox.setModel(new DefaultComboBoxModel<>(motherNodes.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
				});


		chooseImageButton.addActionListener(e -> {
			JFileChooser jFileChooser = new JFileChooser();
			jFileChooser.setAcceptAllFileFilterUsed(false);
			String[] typeImage = ImageIO.getReaderFileSuffixes();
			jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					"Image files : " + Arrays.toString(typeImage), typeImage));
			int tmp = jFileChooser.showDialog(null, "Choose image");
			if (tmp == JFileChooser.APPROVE_OPTION) {
				file = jFileChooser.getSelectedFile();
				chooseImageButton.setText(file.getName());
			}
		});


		sexBox.addItemListener(e -> {
			Sex sex = Sex.values()[
					sexBox.getSelectedIndex()
					];
			if (Sex.Man.equals(sex)) {
				partnerBox.setModel(new DefaultComboBoxModel<>(motherNodes.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
				if (partnerBox.getSelectedIndex() != -1) partnerBox.setSelectedIndex(0);
			} else if (Sex.Woman.equals(sex)) {
				partnerBox.setModel(new DefaultComboBoxModel<>(fatherNodes.stream().map(x -> String.format("%s %s %s", x.name, x.surname, x.firstName)).toArray()));
				if (partnerBox.getSelectedIndex() != -1) partnerBox.setSelectedIndex(0);
			}

		});


		final JTextComponent tc = (JTextComponent) motherBox.getEditor().getEditorComponent();
		tc.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(
						() -> {
							Object s = lookupItem(tc.getText(), motherNodes);
							if (s != null) motherBox.setSelectedItem(s);
						}

				);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(
						() -> {
							Object s = lookupItem(tc.getText(), motherNodes);
							if (s != null) motherBox.setSelectedItem(s);
						}

				);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {

			}
		});


		final JTextComponent tc1 = (JTextComponent) fatherBox.getEditor().getEditorComponent();
		tc1.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(
						() -> {
							Object s = lookupItem(tc1.getText(), fatherNodes);
							if (s != null) fatherBox.setSelectedItem(s);
						}
				);

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(
						() -> {
							Object s = lookupItem(tc1.getText(), fatherNodes);
							if (s != null) fatherBox.setSelectedItem(s);
						}
				);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {

			}
		});


		addButton.addActionListener(e -> {
			String name = namePersonTextField.getText();
			int age = Integer.parseInt(ageField.getText());
			if (name.isEmpty())
				return;

			String surName = surnameField.getText();
			String firstName = firstNameField.getText();
			String profession = professionField.getText();


			Sex sex = Sex.values()[
					sexBox.getSelectedIndex()
					];
			Node mother = (motherBox.getSelectedIndex() == -1) ? null : motherNodes.get(motherBox.getSelectedIndex());
			Node father = (fatherBox.getSelectedIndex() == -1) ? null : fatherNodes.get(fatherBox.getSelectedIndex());
			Node partner = (partnerBox.getSelectedIndex() == -1) ?
					null :
					(Sex.Woman.equals(sex) ?
							fatherNodes.get(partnerBox.getSelectedIndex()) :
							motherNodes.get(partnerBox.getSelectedIndex())
					);


			String nameFile = MainFrame.UNKNOWN_IMAGE_PATH;
			if (file != null) {
				try {
					Files.copy(file.toPath(), new FileOutputStream(PATH + file.getName()));
					nameFile = PATH + file.getName();
				} catch (IOException e1) {
					nameFile = MainFrame.UNKNOWN_IMAGE_PATH;
				}
			}
			String dateBorn = dateBornField.getText();
			dateBorn = (dateBorn.isEmpty()) ? null : dateBorn;
			Node node = new Node(name, surName, firstName, -1, age, profession, partner, mother, father, sex, nameFile, dateBorn);
			if (!check(node))
				return;
			tree.addNewPerson(node);
			setVisible(false);
			dispose();

		});

		pack();
		setVisible(true);
	}

	private Object lookupItem(String name, List<Node> nodes) {
		String searchName = name.toLowerCase();
		for (Node node : nodes) {
			String s = node.getName().toLowerCase();
			if (s.startsWith(searchName))
				return node.getName();
		}
		return null;
	}

	private boolean check(Node node) {
		final int MAX_AGE = 100;
		Node mother, father, partner;
		boolean flagErrorMotherAge = false, flagErrorFatherAge = false, flagErrorPartnerAge = false;
		boolean flagErrorDate = false;
		mother = node.mother;
		father = node.father;
		partner = node.partner;
		String dateBorn = node.dateBorn;

		flagErrorDate = !checkDate(dateBorn);

		if (!flagErrorDate && dateBorn != null) {
			if (partner != null && partner.dateBorn != null && MAX_AGE <= Math.abs(differenceAge(dateBorn, partner.dateBorn)))
				flagErrorPartnerAge = true;

			int tmp = 0;
			if (mother != null && mother.dateBorn != null) {
				tmp = differenceAge(dateBorn, mother.dateBorn);
				if (MAX_AGE <= tmp || tmp <= 0)
					flagErrorMotherAge = true;
			}

			if (father != null && father.dateBorn != null) {
				tmp = differenceAge(dateBorn, father.dateBorn);
				if (MAX_AGE <= tmp || tmp <= 0)
					flagErrorFatherAge = true;
			}
		}

		boolean flagErrorPartnerEqualsMother = false,
				flagErrorPartnerEqualsFather = false;


		if (partner != null && partner.equals(mother)) {
			flagErrorPartnerEqualsMother = true;
		}
		if (partner != null && partner.equals(father)) {
			flagErrorPartnerEqualsFather = true;
		}


		if (flagErrorFatherAge || flagErrorMotherAge || flagErrorPartnerAge || flagErrorDate || flagErrorPartnerEqualsFather || flagErrorPartnerEqualsMother) {
			StringBuilder builder = new StringBuilder("<html>");
			builder.append("Error<br>");
			if (flagErrorDate) builder.append("Wrong date<br>");
			if (flagErrorFatherAge) builder.append("Selected father is too old<br>");
			if (flagErrorMotherAge) builder.append("Selected mother is too old<br>");
			if (flagErrorPartnerAge) builder.append("Selected partner is too old<br>");
			if (flagErrorPartnerEqualsFather) builder.append("Partner is Father<br>");
			if (flagErrorPartnerEqualsMother) builder.append("Partner is Mother<br>");


			builder.append("</html>");
			errorLabel.setText(builder.toString());
			pack();
			revalidate();
			return false;

		}

		return true;

	}

	public AddPersonDialog(Tree tree, String who, Node node) {
		this(tree);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (who != null) {
					switch (who) {
						case CHILD:
							if (node.sex.equals(Sex.Woman))
								motherBox.setSelectedIndex(motherNodes.indexOf(node));
							else
								fatherBox.setSelectedIndex(fatherNodes.indexOf(node));

							break;
						case PARTNER:
							if (node.sex.equals(Sex.Woman)) {
								int tmp = motherNodes.indexOf(node);
								partnerBox.setSelectedIndex(tmp);
							} else {
								sexBox.setSelectedIndex(1);
								int tmp = fatherNodes.indexOf(node);
								partnerBox.setSelectedIndex(tmp);
							}
							break;
					}
				}
			}
		});
	}


	private boolean checkDate(String date) {
		return date == null || date.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}");
	}

	private int differenceAge(String date1, String date2) {
		int first = Integer.parseInt(date1.split("/")[2]);
		int second = Integer.parseInt(date2.split("/")[2]);
		return first - second;
	}


}
