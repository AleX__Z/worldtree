package com.company.gui;

import com.company.lang.Node;
import com.company.lang.Tree;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Alex on 28.12.2014.
 */
public class PersonFrame extends JFrame {
	private JLabel photoLabel;
	private JLabel infoLabel;
	private JPanel personPanel;
	private JButton addMotherButton;
	private JButton addFatherButton;
	private JButton addPartnerButton;
	private JButton addChildButton;

	public PersonFrame(Tree tree, Node person) {
		super("");
		setContentPane(personPanel);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Person");

		photoLabel.setIcon(getResizeImageIcon(new ImageIcon(person.nameFilePhoto)));
		infoLabel.setText(person.toString());

		if (person.partner != null)
			addPartnerButton.setVisible(false);

		addChildButton.addActionListener(e -> new AddPersonDialog(tree, AddPersonDialog.CHILD, person));
		addPartnerButton.addActionListener(e -> new AddPersonDialog(tree, AddPersonDialog.PARTNER, person));

		addMotherButton.setVisible(false);
		addFatherButton.setVisible(false);

		pack();
		setVisible(true);
	}

	private static ImageIcon getResizeImageIcon(ImageIcon icon) {
		Image image = icon.getImage();
		return new ImageIcon(image.getScaledInstance(100, 100, Image.SCALE_SMOOTH));
	}

}
