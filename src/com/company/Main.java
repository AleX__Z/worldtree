package com.company;

import com.company.lang.Node;
import com.company.lang.Sex;
import com.company.lang.Tree;
import com.company.gui.MainFrame;

import javax.swing.*;

public class Main {

	/*public static void test() {
		Tree tree = new Tree();
		tree
				.addNewPerson(new Node("Adam", Sex.Man))
				.addNewPerson(new Node("Eva", Sex.Woman));
		tree.save();
	}*/

	public static void main(String[] args) {
		Tree tree = Tree.load();
		if (tree == null) tree = new Tree();
		new MainFrame(tree);
	}
}
