package com.company.lang;

import com.company.gui.MainFrame;

import java.io.*;
import java.util.*;

public class Tree implements Serializable {
	public static final String DEFAULT_FILE_NAME = "tree.tree";
	public static final String NAME_ROOT = "Krauser II";
	public static final String SURNAME_ROOT = "Johannes";
	public static final String FIRST_NAME_ROOT = "DMC";


	public int size;
	Node root;

	public Tree() {
		this.root = new Node(
				NAME_ROOT,
				SURNAME_ROOT,
				FIRST_NAME_ROOT,
				0,
				23,
				"",
				null,
				null,
				null,
				null,
				MainFrame.UNKNOWN_IMAGE_PATH, null
		);
		size = 0;
	}

	public static Tree load(String name) {
		Tree tree = null;
		try (
				ObjectInput in =
						new ObjectInputStream(
								new BufferedInputStream(
										new FileInputStream(name)))
		) {
			tree = (Tree) in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return tree;
	}

	public Tree save(String name) {
		try (
				ObjectOutput out =
						new ObjectOutputStream(
								new BufferedOutputStream
										(new FileOutputStream(name)))
		) {
			out.writeObject(this);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return this;
	}

	public static Tree load() {
		return Tree.load(DEFAULT_FILE_NAME);
	}

	public Tree save() {
		return save(DEFAULT_FILE_NAME);
	}

	public List<Node> findPersonByName(String name) {
		ArrayList<Node> result = new ArrayList<>();
		if (name == null || size == 0)
			return result;

		String s = name.toLowerCase();
		Queue<Node> queue = new ArrayDeque<>();
		queue.addAll(root.children);
		Node q;
		boolean[] used = new boolean[size + 1];
		used[root.id] = true;

		while (!queue.isEmpty()) {
			q = queue.poll();
			if (used[q.id]) continue;
			if (q.name.toLowerCase().contains(s)) result.add(q);
			queue.addAll(q.children);
			used[q.id] = true;
		}

		return result;
	}


	public Tree addNewPerson(Node person) {
		if (person.mother == null && person.father == null) {
			root.addChild(person);
		}
		if (person.partner != null) person.partner.partner = person;

		if (person.mother != null)
			person.mother.addChild(person);
		if (person.father != null)
			person.father.addChild(person);
		size++;
		person.id = size;

		return this;
	}

	public Tree deleteNode(Node node) {
		if (node.father == null && node.mother == null)
			root.children.remove(node);
		if (node.father != null)
			node.father.children.remove(node);
		if (node.mother != null)
			node.mother.children.remove(node);

		if (node.partner != null)
			node.partner.partner = null;

		for (Node c : node.children) {
			if (Sex.Woman.equals(node.sex))
				c.mother = null;
			else c.father = null;
			if (c.father == null && c.mother == null)
				root.addChild(c);
		}

		return this;
	}

}
