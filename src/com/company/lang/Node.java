package com.company.lang;

import java.io.Serializable;
import java.util.*;

public class Node implements Serializable {
	public int id;
	public String name;
	public String surname;
	public String firstName;
	public int age;
	public String profession;
	public Node partner;
	public Node mother;
	public Node father;
	public Set<Node> children;
	public Sex sex;
	public String nameFilePhoto;
	public String dateBorn;


	{
		children = new HashSet<>();
	}

	public Node(String name,
	            String surname,
	            String firstName,
	            int id,
	            int age,
	            String profession,
	            Node partner,
	            Node mother,
	            Node father,
	            Sex sex,
	            String nameFilePhoto, String dateBorn) {
		this.name = name;
		this.surname = surname;
		this.firstName = firstName;
		this.id = id;
		this.age = age;
		this.profession = profession;
		this.partner = partner;
		this.mother = mother;
		this.father = father;
		this.sex = sex;
		this.nameFilePhoto = nameFilePhoto;
		this.dateBorn = dateBorn;
	}

	/*public Node(String name, int id, Sex sex, Node mother, Node father) {
		this.name = name;
		this.sex = sex;
		this.id = id;
		this.mother = mother;
		this.father = father;
		nameFilePhoto = MainFrame.UNKNOWN_IMAGE_PATH;
	}*/

	/*public Node(String name, Sex sex, Node mother, Node father) {
		this(name, -1, sex, mother, father);
	}

	public Node(String name, Sex sex) {
		this(name, -1, sex, null, null);
	}

	*/
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isWoman() {
		return Sex.Woman.equals(sex);
	}

	public boolean isMan() {
		return Sex.Man.equals(sex);
	}

	public List<Node> getParents() {
		return Arrays.asList(mother, father);
	}


	public Node addChild(Node child) {
		if (Sex.Man.equals(sex))
			child.father = this;
		else if (Sex.Woman.equals(sex))
			child.mother = this;
		this.children.add(child);
		return this;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("<html>");
		builder.append(String.format("%s %s %s<br>", name, surname, firstName));
		builder.append("Date of birth : ").append(dateBorn).append("<br>");
		builder.append("Age : ").append(age).append("<br>");
		builder.append("Sex : ").append(sex).append("<br>");
		builder.append("Profession : ").append(profession).append("</html>");
		return builder.toString();
	}
}
