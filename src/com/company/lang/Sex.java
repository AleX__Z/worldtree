package com.company.lang;

import java.io.Serializable;

public enum Sex implements Serializable {
	Man, Woman
}
